package sample;

import javafx.scene.Node;

public interface Controller {
  public Node getView();
}
