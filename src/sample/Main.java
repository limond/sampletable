package sample;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.Observable;

public class Main extends Application {

  @Override
  public void start(Stage primaryStage) throws Exception {
    primaryStage.setTitle("Hello World");
    VBox pane = new VBox();
    TableController table = new TableController();
    Node view = table.getView();
    CheckBox controlCounterexamples = new CheckBox("Counterexamples");
    pane.getChildren().addAll(controlCounterexamples, view);
    primaryStage.setScene(new Scene(pane, 600, 400));
    primaryStage.show();

    table.addColumnInput();
    table.addColumnInput();
    table.addColumnInput();

    table.addColumnOutput();
    table.addColumnOutput();
    table.addColumnOutput();

    table.addRow();
    table.addRow();
    table.addRow();
    table.addRow();
    table.addColumnInput();

    controlCounterexamples.onActionProperty().setValue(action -> {
      if(controlCounterexamples.isSelected()){
        table.pseudoCounterexamples();
      }
      else{
        table.clearCounterexamples();
      }
    });
  }


  public static void main(String[] args) {
    launch(args);
  }
}
