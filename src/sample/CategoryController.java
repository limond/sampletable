package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

/**
 * Created by leonk on 25.01.2017.
 */
public class CategoryController implements Controller{
  private VBox mainView = new VBox();
  private Text text = new Text();
  private HBox columnContainer = new HBox();
  private ObservableList<Node> columns = columnContainer.getChildren();
  public CategoryController(String headline){
    text.setText(headline);
    mainView.getChildren().addAll(text, columnContainer);
  }

  @Override
  public Node getView() {
    return mainView;
  }

  public ObservableList<Node> getColumns() {
    return columns;
  }
}
