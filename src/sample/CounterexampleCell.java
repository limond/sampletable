package sample;

import javafx.geometry.Pos;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;

/**
 * Created by leonk on 25.01.2017.
 */
public class CounterexampleCell extends HBox{
  public static int CellHeight = 50;
  public CounterexampleCell(String string) {
    super();
    setAlignment(Pos.CENTER);
    setPrefHeight(CellHeight);
    getChildren().add(new Text(string));
  }
}
