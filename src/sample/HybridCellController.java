package sample;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

/**
 * Created by leonk on 25.01.2017.
 */
public class HybridCellController implements Controller{
  VBox mainView = new VBox();
  VBox counterexamples = new VBox();
  public HybridCellController() {
    TextField value = new TextField();
    value.setPrefHeight(50);
    mainView.getChildren().addAll(value, counterexamples);
    mainView.setStyle("-fx-border-color: black; -fx-border-width: 0 0 1 0;");
  }

  @Override
  public Node getView() {
    return mainView;
  }

  public ObservableList<Node> getCounterexamples(){
    return counterexamples.getChildren();
  }
}
