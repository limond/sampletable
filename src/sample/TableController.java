package sample;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.layout.HBox;

/**
 * Created by leonk on 25.01.2017.
 */
public class TableController implements Controller {
  private HBox mainBox;
  private final CategoryController input;
  private final CategoryController output;
  private final ObservableList<TableColumnConroller> columns = FXCollections.observableArrayList();
  private int rows = 0;

  public TableController() {
    mainBox = new HBox();
    input = new CategoryController("Input");
    output = new CategoryController("Output");
    mainBox.getChildren().addAll(input.getView(), output.getView());
  }

  @Override
  public Node getView() {
    return mainBox;
  }

  public TableColumnConroller addColumnInput() {
    TableColumnConroller tableColumnConroller = new TableColumnConroller();
    makeDragable(tableColumnConroller, input);
    input.getColumns().add(tableColumnConroller.getView());
    columns.add(tableColumnConroller);
    for (int i = 0; i < rows; i++) {
      tableColumnConroller.addCell();
    }
    return tableColumnConroller;
  }

  public TableColumnConroller addColumnOutput() {
    TableColumnConroller tableColumnConroller = new TableColumnConroller();
    makeDragable(tableColumnConroller, output);
    columns.add(tableColumnConroller);
    output.getColumns().add(tableColumnConroller.getView());
    for (int i = 0; i < rows; i++) {
      tableColumnConroller.addCell();
    }
    return tableColumnConroller;
  }

  public void addRow() {
    columns.forEach(TableColumnConroller::addCell);
    rows++;
  }

  public void pseudoCounterexamples() {
    clearCounterexamples();
    columns.stream().forEach(column -> column.getCells()
        .stream().forEach(cell -> {
              cell.getCounterexamples().addAll(new CounterexampleCell("LOL1"), new CounterexampleCell("LOL2"));
            }
        )
    );
  }
  public void clearCounterexamples(){
    columns.stream().forEach(column -> column.getCells()
        .stream().forEach(cell -> {
              cell.getCounterexamples().clear();
            }
        )
    );
  }

  private void makeDragable(TableColumnConroller column, CategoryController category) {
    column.setOnColumnDrag((x, y) -> {
      category.getColumns().remove(column.getView());
      double xSpace = category.getView().localToScene(category.getView().getBoundsInLocal()).getMinX();
      int i = 0;
      while (xSpace < x && i < category.getColumns().size()) {
        Node node = category.getColumns().get(i);
        xSpace += node.getBoundsInLocal().getWidth();
        i++;
        System.out.println(xSpace);
      }
      category.getColumns().add(i, column.getView());
    });
  }
}
