package sample;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Bounds;
import javafx.geometry.Pos;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;

import java.util.function.BiConsumer;

/**
 * Created by leonk on 25.01.2017.
 */
public class TableColumnConroller implements Controller{
  private VBox mainView = new VBox();
  private HBox header = new HBox();
  private VBox cells = new VBox();
  private ObservableList<HybridCellController> cellController = FXCollections.observableArrayList();

  public TableColumnConroller(){
    mainView.setPrefWidth(50);
    Text headerText = new Text("Header");
    this.header.getChildren().addAll(headerText);
    this.header.setAlignment(Pos.CENTER);
    this.header.setPrefHeight(50);
    this.header.setStyle("-fx-background-color: #F00");
    mainView.getChildren().addAll(this.header, cells);
    mainView.setStyle("-fx-border-color: black; -fx-border-width: 0 2 0 0;");
    mainView.setPrefWidth(200);
  }

  @Override
  public VBox getView() {
    return mainView;
  }

  public ObservableList<HybridCellController> getCells(){
    return cellController;
  }

  public void setOnColumnDrag(BiConsumer<Double, Double> consumer){
    DoubleProperty offsetX = new SimpleDoubleProperty();
    DoubleProperty offsetY = new SimpleDoubleProperty();
    this.header.setOnDragDetected((MouseEvent mouseEvent) ->{
      Bounds bounds = mainView.localToScene(mainView.getBoundsInLocal());
      offsetX.setValue(mouseEvent.getSceneX()-bounds.getMinX());
      offsetY.setValue(mouseEvent.getSceneY()-bounds.getMinY());
    });
    this.header.setOnMouseMoved(mouseEvent -> {

    });
    this.header.setOnMouseReleased(dragEvent -> {
      consumer.accept(dragEvent.getSceneX()-offsetX.get(), dragEvent.getSceneY()-offsetY.get());
    });
  }

  public void addCell() {
    HybridCellController hybridCellController = new HybridCellController();
    cellController.add(hybridCellController);
    cells.getChildren().add(hybridCellController.getView());
  }
}
